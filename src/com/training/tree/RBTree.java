package com.training.tree;

import java.util.LinkedList;
import java.util.Queue;

public class RBTree {

    private Node root;

    public static final String ANSI_RED = "\u001B[31m";

    public static final String ANSI_RESET = "\u001B[0m";

    public RBTree insert(String key) {
        root = put(key, root);
        root.color = true;
        //System.out.println("********  root " + root.key + "   " + root.color);
        return this;
    }

    private Node put(String key, Node node) {
        if (node == null) {
            return new Node(key);
        } else if (node.key().compareTo(key) < 0) {
            node.right = put(key, node.right);
        } else {
            node.left = put(key, node.left);
        }
        //System.out.println(node.key + "   " + key);
        if (!isRed(node.left)  && isRed(node.right)) {
            //System.out.println("rotate left called for " + node.key);
            node = rotateLeft(node);

            //System.out.println("after rotate left " + node.key + "  "  + node.color + "   " + node.left.color + "  " + node.right.color);
        }
        if (isRed(node.left) && isRed(node.left.left)) {
            node = rotateRight(node);
            //System.out.println("after rotate right " + node.key + "  "  + node.color + "   " + node.left.color + "  " + node.right.color);
        }
        if (isRed(node.left) && isRed(node.right)) {
            //System.out.println("flip colors called for " + node.key);
            node = flipColors(node);
            //System.out.println(node.key + " " + node.color);
            //System.out.println(node.left.key + " " + node.left.color);
            //System.out.println(node.right.key + " " + node.right.color);
        }
        return node;
    }

    private boolean isRed(Node node) {
        return node != null && node.isRed();
    }

    private Node flipColors(Node node) {
        node.left.flipColor();
        node.right.flipColor();
        node.color = false;
        return node;
    }

    private Node rotateRight(Node node) {
        Node temp = node.left;
        node.left = temp.right;
        temp.right = node;
        //node.color = temp.color;
        boolean color = node.color;
        node.color = temp.color;
        temp.color = color;
        return temp;
    }

    private Node rotateLeft(Node node) {
        Node temp = node.right;
        node.right = temp.left;
        temp.left = node;
        boolean color = node.color;
        node.color = temp.color;
        temp.color = color;
        return temp;
    }

    private static class Node {
        private String key;
        private Node left, right;
        private boolean color;
        protected int level = 1;

        public Node(String key) {
            this.key = key;
        }

        public boolean isRed() {
            return color == false; //we assume default is red.
        }

        public void flipColor() {
            this.color = !color;
        }

        public String key() {
            return this.key;
        }

        public boolean isNull() {
            return false;
        }
    }

    private static class NullNode extends Node {

        public NullNode() {
            super(null);
        }

        @Override
        public boolean isNull() {
            return true;
        }

    }

    public void prettyPrint() {
        bfs(this.root);
    }

    //just do a breadth first search and attempt to pretty print
    private void bfs(Node node) {
        Queue<Node> queue = new LinkedList<>();
        queue.offer(node);
        Node n = null;
        int l = 0;
        int startSpace = 0;
        while ( (n = queue.poll()) != null) {
            if (n.level > l) {
                System.out.println("\n\n\n");
                l = n.level;
                startSpace = Math.floorDiv(160, (1 << l));
                for(int i=0;i<startSpace; i++) System.out.print(" ");
                //for(int i=startSpace/2 + 1 ;i<startSpace; i++) System.out.print("_");
            }

            if (n.key != null) {
                if (n.isRed()) System.out.print(ANSI_RED + n.key + ANSI_RESET); else
                System.out.print(n.key);
            } else {
                System.out.print("*");
            }
            int intSpace = startSpace * 2;
            for(int i=0;i<intSpace; i++) System.out.print(" ");
            //for(int i=0;i<startSpace/2; i++) System.out.print("_");


            final NullNode nullNode = new NullNode();
            if(n.left != null) {
                n.left.level = n.level + 1;
                queue.offer(n.left);
            } else  if (n.left == null && !n.isNull()){
                nullNode.level = n.level + 1;
                queue.offer(nullNode);
            }
            if(n.right != null) {
                n.right.level = n.level + 1;
                queue.offer(n.right);
            } else if (n.right == null && !n.isNull()){
                nullNode.level = n.level + 1;
                queue.offer(nullNode);
            }
        }
    }

    public static void main(String[] args) {
        RBTree tree = new RBTree();
        tree.insert("S")
            .insert("E")
            .insert("A")
            .insert("R")
            .insert("C")
            .insert("H")
            .insert("X")
            .insert("M")
            .insert("P")
            .insert("L")
            .prettyPrint();

        /*tree.insert("1")
                .insert("2")
                .insert("3")
                .insert("4")
                .insert("5")
                .insert("6")
                .insert("7")
                .insert("8")
                .insert("9")
                .insert("10")
                .prettyPrint();*/
        //System.out.println("tree " + tree);
    }
}
